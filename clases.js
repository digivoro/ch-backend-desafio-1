class Usuario {
  constructor(
    nombre = "Nomen",
    apellido = "Nescio",
    libros = [],
    mascotas = []
  ) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.libros = libros;
    this.mascotas = mascotas;
  }

  getFullName() {
    return `${this.nombre} ${this.apellido}`;
  }

  addMascota(nombreMascota) {
    this.mascotas.push(nombreMascota);
  }

  countMascotas() {
    return this.mascotas.length;
  }

  addBook(nombre, autor) {
    this.libros.push({ nombre, autor });
  }

  getBookNames() {
    return this.libros.map((libro) => libro.nombre);
  }
}

const usuario = new Usuario("Felipe", "Castro");
usuario.addBook("Luna de plutón", "Dross Rotzank");
usuario.addMascota("Bola de Nieve IV");
usuario.addMascota("Ayudante de Santa");

console.log("Nombre completo: ", usuario.getFullName());
console.log("Cantidad de mascotas:", usuario.countMascotas());
console.log("Lista de libros:", usuario.getBookNames());
